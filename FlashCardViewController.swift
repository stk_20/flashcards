//
//  FlashCardViewController.swift
//  Flashcards
//
//  Created by Stephen Kelly on 1/26/16.
//  Copyright © 2016 Bitchnwuss. All rights reserved.
//

import UIKit
import AVFoundation

class Flashcard {
    var english: String
    var foreign: String
    var audioFile: String
    var correct: Bool?
    
    init(english: String, foreign: String, audioFile: String) {
        self.english = english
        self.foreign = foreign
        self.audioFile = audioFile
    }
}

var deck: [Flashcard] = [
    Flashcard(english: "elder sister; you", foreign: "chị", audioFile: "01-04_Vocabulary_Dialog_1_chị_M"),
    Flashcard(english: "elder brother; you", foreign: "anh",	audioFile: "01-04_Vocabulary_Dialog_1_anh_F"),
    Flashcard(english: "I", foreign: "tôi", audioFile: "01-04_Vocabulary_Dialog_1_tôi_M"),
    Flashcard(english: "also, too", foreign: "cũng", audioFile: "01-04_Vocabulary_Dialog_1_cũng_F"),
    Flashcard(english: "very", foreign: "rãt", audioFile: "01-04_Vocabulary_Dialog_1_rãt_M")
]

class FlashCardViewController: UIViewController {
    @IBOutlet var flashcardView: UIView!
    @IBOutlet var frontView: UIView!
    @IBOutlet var backView: UIView!
    
    @IBOutlet var frontLabel: UILabel!
    @IBOutlet var backLabel: UILabel!
    
    @IBOutlet var cardNumberLabel: UILabel!
    @IBOutlet var correctPercentLabel: UILabel!
    @IBOutlet var streakLabel: UILabel!
    
    var showingBack = true
    var deckIndex = 0
    var streak = 0
    
    var audioPlayer: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let singleTap = UITapGestureRecognizer(target: self, action: Selector("tapped"))
        singleTap.numberOfTapsRequired = 1
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeDown)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeUp.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        flashcardView.addGestureRecognizer(singleTap)
        flashcardView.userInteractionEnabled = true
        
        backLabel.text = deck[deckIndex].english
        frontLabel.text = deck[deckIndex].foreign
        
        updateScoreLabels()
    }
    
    func tapped() {
        if (showingBack) {
            UIView.transitionFromView(backView, toView: frontView, duration: 1, options: [.TransitionFlipFromRight, .ShowHideTransitionViews], completion: nil)
            showingBack = false
        } else {
            UIView.transitionFromView(frontView, toView: backView, duration: 1, options: [.TransitionFlipFromLeft, .ShowHideTransitionViews], completion: nil)
            showingBack = true
        }
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Down:
                print("Swiped down")
                deck[deckIndex].correct = false
                streak = 0
                nextCard()
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
                deck[deckIndex].correct = true
                streak++
                nextCard()
            case UISwipeGestureRecognizerDirection.Left:
                print("Swiped left")
                nextCard()
            case UISwipeGestureRecognizerDirection.Right:
                print("Swiped right")
                previousCard()
            default:
                break
            }
        }
    }
    
    // MARK: Utilities
    func nextCard() {
        if  deckIndex < deck.count - 1 {
            deckIndex++
            updateScoreLabels()
            
            backLabel.text = deck[deckIndex].english
            if !showingBack {
                UIView.transitionFromView(frontView, toView: backView, duration: 1, options: [.TransitionNone, .ShowHideTransitionViews], completion: nil)
                showingBack = true
            }
            flashcardView.slideInFromRight()
            showingBack = true
            frontLabel.text = deck[deckIndex].foreign
        }
    }
    
    func previousCard() {
        if  deckIndex > 0 {
            deckIndex--
            updateScoreLabels()
            
            backLabel.text = deck[deckIndex].english
            if !showingBack {
                UIView.transitionFromView(frontView, toView: backView, duration: 1, options: [.TransitionNone, .ShowHideTransitionViews], completion: nil)
                showingBack = true
            }
            flashcardView.slideInFromLeft()
            showingBack = true
            frontLabel.text = deck[deckIndex].foreign
        }
    }
    
    func updateScoreLabels() {
        cardNumberLabel.text = "\(deckIndex + 1) / \(deck.count)"
        if correctPercentage() > 0 {
            correctPercentLabel.text = String(format: "%.0f", correctPercentage()) + " % correct"
        } else {
            correctPercentLabel.text = ""
        }
        streakLabel.text = "streak \(streak)"
    }
    
    func correctPercentage() -> Double {
        var totalAnswered = 0.0
        var totalCorrect = 0.0
        for card in deck {
            if card.correct != nil {
                totalAnswered++
                totalCorrect += card.correct! ? 1.0 : 0.0
            }
        }
        if totalAnswered > 0 {
            return totalCorrect / totalAnswered * 100
        } else {
            return -1
        }
    }
    
    @IBAction func playTranslation(sender: AnyObject) {
        let audio = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("Audio/" + deck[deckIndex].audioFile, ofType: "mp3")!)
        do{
            audioPlayer = try AVAudioPlayer(contentsOfURL:audio)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }catch {
            print("Error getting the audio file")
        }
    }
}


// Magic that I straight up copied
extension UIView {
    // Name this function in a way that makes sense to you...
    // slideFromLeft, slideRight, slideLeftToRight, etc. are great alternative names
    func slideInFromLeft(duration: NSTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: AnyObject = completionDelegate {
            slideInFromLeftTransition.delegate = delegate
        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.addAnimation(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
    
    func slideInFromRight(duration: NSTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: AnyObject = completionDelegate {
            slideInFromLeftTransition.delegate = delegate
        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromRight
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.addAnimation(slideInFromLeftTransition, forKey: "slideInFromRightTransition")
    }
}
